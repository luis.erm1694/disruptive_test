import 'package:disruptive_lerm/app/data/model/login_response.dart';
import 'package:get_storage/get_storage.dart';

class GlobalMemory {

  /// Almacena los datos del usuario
  static LoginResponse? user;
  static GetStorage? box;
}