
import 'dart:convert';
import 'package:disruptive_lerm/app/data/model/general_model.dart';
import 'package:disruptive_lerm/app/data/model/login_response.dart';
import 'package:disruptive_lerm/app/data/provider/global_data.dart';
import 'package:http/http.dart' as http;
import 'package:disruptive_lerm/app/data/model/address_model.dart';
import 'package:disruptive_lerm/app/data/model/all_address_response.dart';
import 'package:disruptive_lerm/app/data/model/postal_code_model.dart';
import 'package:disruptive_lerm/app/data/provider/paths_provider.dart';
import 'package:get/get.dart';



class DataProvider extends GetConnect {

  PathProvider pathProvider = PathProvider();

  Future <GeneralModel>register({required String user, required String lastname, required String email, required String password}) async {
    String url = '${PathProvider.baseUrl}${PathProvider.register}';
    final payload = jsonEncode({
      'nombre': user, 
      'apellidos': password,
      'correo': email,
      'password': password
    });
    final response = await post(url, payload);
    
    return GeneralModel.fromJson(response.body);
  }
  
  Future<LoginResponse>login({required String user, required String password}) async {
    String url = '${PathProvider.baseUrl}${PathProvider.login}';
    final payload = jsonEncode({
      'Usuario': user, 
      'password': password
    });
    final response = await post(url, payload);
    final jsonString = json.encode(response.body);
    GlobalMemory.box!.write('user', jsonString);
    GlobalMemory.user = LoginResponse.fromJson(response.body);
    return LoginResponse.fromJson(response.body);
  }


  Future<PostalCodeResponseModel> searchByCp({required String postalCode}) async {
    String url = '${PathProvider.postalCodePath}$postalCode';
    var response = await get(url);
    return PostalCodeResponseModel.fromJson(response.body);
  }

  Future addAddress({required AddressModel address, required int idUser}) async {
    String url = '${PathProvider.baseUrl}${PathProvider.postAddress}';
    final payload = jsonEncode({
    "idUser": idUser,
    "dir_calle": address.street,
    "dir_NumExt": address.externalNumber.toString(),
    "dir_cp": address.postalCode,
    "dir_estado": address.state,
    "dir_municipio": address.municipality,
    "dir_colonia": address.colony,
    "dir_referencias": address.references,
    "dir_isDefault": address.isDefault
});
    await post(url, payload);
  }

  Future<GeneralModel>updateAddress({required AddressModel address}) async {
    String url = '${PathProvider.baseUrl}${PathProvider.updateAddress}';
    final payload = jsonEncode({
      "direccionId": address.id,
      "dir_calle": address.street,
      "dir_NumExt": address.externalNumber.toString(),
      "dir_cp": address.postalCode,
      "dir_estado": address.state,
      "dir_municipio": address.municipality,
      "dir_colonia": address.colony,
      "dir_referencias": address.references,
      "dir_isDefault": address.isDefault
    });
    final response = await post(url, payload);
    return  GeneralModel.fromJson(response.body);
  }

  Future<AllAddresResponse> getAddress({required String userId}) async {
    String url = '${PathProvider.baseUrl}${PathProvider.getAddress}$userId';
    var response = await get(url);
    return AllAddresResponse.fromJson(response.body);
  }

  Future deleteAddress({required String addressId}) async {
  final url = Uri.parse('https://rocketsapp.com.mx/kike/direccion_user_delete.php');
  final body = {
      "direccionId": addressId
    };
  final response = await http.delete(url, body: jsonEncode(body) );
  return response;
}

}