class PathProvider{
  
  static const String login =  '/login_user.php';
  static const String register ='/usuario_alta.php';
  static const String baseUrl = 'https://rocketsapp.com.mx/kike';
  static const String postalCodePath = 'https://nowmobile.mx/sepomex-get/';
  static const String getAddress = '/direccion_user_id.php?userid=';
  static const String postAddress = '/direccion_user.php';
  static const String updateAddress = '/direccion_user_update.php';
  static const String deleteAddress = '/direccion_user_delete.php';
}