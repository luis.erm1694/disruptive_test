import 'dart:convert';

class AddressModel {
  AddressModel(
   { 
    required this.id,
    required this.street, 
    required this.externalNumber, 
    required this.postalCode, 
    required this.state, 
    required this.municipality, 
    required this.colony, 
    required this.references, 
    required this.isDefault,
    this.idUSer
   }
  );
  
  final String id;
  final String? idUSer;
  final String street;
  final String externalNumber;
  final String postalCode;
  final String state;
  final String municipality;
  final String colony;
  final String references;
  final String isDefault;

  factory AddressModel.fromRawJson(String str) => AddressModel.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory AddressModel.fromJson(Map<String, dynamic> json) => AddressModel(
        id: json["id_dir"],
        idUSer: json["dir_id_use"],
        street: json["dir_calle"],
        externalNumber: json["dir_NumExt"],
        postalCode: json["dir_cp"],
        state: json["dir_estado"],
        municipality: json["dir_municipio"],
        colony: json["dir_colonia"],
        references: json["dir_referencias"],
        isDefault: json["dir_isDefault"],
    );

    Map<String, dynamic> toJson() => {
        "id_dir": id,
        "dir_id_use": idUSer,
        "dir_calle": street,
        "dir_NumExt": externalNumber,
        "dir_cp": postalCode,
        "dir_estado": state,
        "dir_municipio": municipality,
        "dir_colonia": colony,
        "dir_referencias": references,
        "dir_isDefault": isDefault,
    };
  

  
  AddressModel copyWith({
  final String? id,
  final String? idUser,
  final String? name,
  final String? street,
  final String? externalNumber,
  final String? postalCode,
  final String? state,
  final String? municipality,
  final String? colony,
  final String? references,
  final String? isDefault,

  }){
    return AddressModel(
      id: id ?? this.id,
      street: street ?? this.street, 
      externalNumber: externalNumber ?? this.externalNumber,
      postalCode: postalCode ?? this.postalCode, 
      state: state ?? this.state, 
      municipality: municipality ?? this.municipality, 
      colony: colony ?? this.colony, 
      references: references ?? this.references, 
      isDefault: isDefault ?? this.isDefault, 
    );
  }

}