// To parse this JSON data, do
//
//     final postalCodeResponseModel = postalCodeResponseModelFromJson(jsonString);

import 'dart:convert';

class PostalCodeResponseModel {
    final List<Sepomex>? sepomex;

    PostalCodeResponseModel({
      this.sepomex,
    });

    factory PostalCodeResponseModel.fromRawJson(String str) => PostalCodeResponseModel.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory PostalCodeResponseModel.fromJson(Map<String, dynamic> json) => PostalCodeResponseModel(
        sepomex: List<Sepomex>.from(json["sepomex"].map((x) => Sepomex.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "sepomex": List<dynamic>.from(sepomex!.map((x) => x.toJson())),
    };
}

class Sepomex {
    final String? id;
    final int? idEstado;
    final String? estado;
    final int? idMunicipio;
    final String? municipio;
    final String? ciudad;
    final String? zona;
    final int? cp;
    final String? asentamiento;
    final String? tipo;

    Sepomex({
        this.id,
        this.idEstado,
        this.estado,
        this.idMunicipio,
        this.municipio,
        this.ciudad,
        this.zona,
        this.cp,
        this.asentamiento,
        this.tipo,
    });

    factory Sepomex.fromRawJson(String str) => Sepomex.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory Sepomex.fromJson(Map<String, dynamic> json) => Sepomex(
        id: json["_id"],
        idEstado: json["idEstado"],
        estado: json["estado"],
        idMunicipio: json["idMunicipio"],
        municipio: json["municipio"],
        ciudad: json["ciudad"],
        zona: json["zona"],
        cp: json["cp"],
        asentamiento: json["asentamiento"],
        tipo: json["tipo"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "idEstado": idEstado,
        "estado": estado,
        "idMunicipio": idMunicipio,
        "municipio": municipio,
        "ciudad": ciudad,
        "zona": zona,
        "cp": cp,
        "asentamiento": asentamiento,
        "tipo": tipo,
    };
}
