// To parse this JSON data, do
//
//     final generalModel = generalModelFromJson(jsonString);

import 'dart:convert';

class GeneralModel {
    final String? code;
    final String? message;

    GeneralModel({
       this.code,
       this.message,
    });

    factory GeneralModel.fromRawJson(String str) => GeneralModel.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory GeneralModel.fromJson(Map<String, dynamic> json) => GeneralModel(
        code: json["code"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
    };
}
