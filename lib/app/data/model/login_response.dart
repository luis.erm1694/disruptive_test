// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

class LoginResponse {
    final String? code;
    final String? message;
    final String? idUser;
    final String? name;
    final String? email;

    LoginResponse({
         this.code,
         this.message,
         this.idUser,
         this.name,
         this.email,
    });

    factory LoginResponse.fromRawJson(String str) => LoginResponse.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        code: json["code"],
        message: json["message"],
        idUser: json["idUser"],
        name: json["Nombre"],
        email: json["Correo"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "idUser": idUser,
        "Nombre": name,
        "Correo": email,
    };
}
