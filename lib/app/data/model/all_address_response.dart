// To parse this JSON data, do
//
//     final allAddresResponse = allAddresResponseFromJson(jsonString);

import 'package:disruptive_lerm/app/data/model/address_model.dart';
import 'dart:convert';

class AllAddresResponse {
    final String? code;
    final String? message;
    final String? name;
    final List<AddressModel>? addressList;

    AllAddresResponse({
         this.code,
         this.message,
         this.name,
         this.addressList,
    });

    factory AllAddresResponse.fromRawJson(String str) => AllAddresResponse.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory AllAddresResponse.fromJson(Map<String, dynamic> json) => AllAddresResponse(
        code: json["code"],
        message: json["message"],
        name: json["Nombre"],
        addressList: List<AddressModel>.from(json["direcciones"].map((x) => AddressModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "Nombre": name,
        "direcciones": List<AddressModel>.from(addressList!.map((x) => x.toJson())),
    };
}

