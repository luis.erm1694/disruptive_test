import 'dart:convert';

import 'package:disruptive_lerm/app/data/model/login_response.dart';
import 'package:disruptive_lerm/app/data/provider/data_provider.dart';
import 'package:disruptive_lerm/app/data/provider/global_data.dart';
import 'package:disruptive_lerm/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SplashController extends GetxController {
  /* -------------------------------------------------------------------------- */
  /*                                 CONTROLLERS                                */
  /* -------------------------------------------------------------------------- */

  /* -------------------------------------------------------------------------- */
  /*                                  VARIABLES                                 */
  /* -------------------------------------------------------------------------- */
  

  /* -------------------------------------------------------------------------- */
  /*                                 LIFECYCLES                                 */
  /* -------------------------------------------------------------------------- */
  
  @override
  void onInit() {
    GlobalMemory.box = GetStorage();
    super.onInit();
  }
  
  @override
  void onReady() {
     Get.lazyPut(() => DataProvider());
    super.onReady();

    _toNextPage();
  }

  /* -------------------------------------------------------------------------- */
  /*                                   METHODS                                  */
  /* -------------------------------------------------------------------------- */

  void _toNextPage() async {
    bool hasSession = false;
    try {
      GlobalMemory.user = LoginResponse.fromJson(jsonDecode(GlobalMemory.box!.read('user')));
      hasSession =  GlobalMemory.user?.name != null;
      
    } catch (_) {      
    }
    
    
    await 2.delay();
    if(hasSession){
      Get.offAllNamed(Routes.homePage);


    }else{
      Get.offAllNamed(Routes.registerPage);

    }
    
  }
}
