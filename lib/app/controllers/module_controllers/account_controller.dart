import 'package:disruptive_lerm/app/data/provider/data_provider.dart';
import 'package:disruptive_lerm/app/routes/app_pages.dart';
import 'package:disruptive_lerm/app/ui/widgets/custom_flushbar.dart';
import 'package:disruptive_lerm/app/utils/common_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AccountController extends GetxController {
  /* -------------------------------------------------------------------------- */
  /*                                 CONTROLLERS                                */
  /* -------------------------------------------------------------------------- */

   TextEditingController emailController = TextEditingController();
   TextEditingController nameController = TextEditingController();
   TextEditingController lastNameController = TextEditingController();
   TextEditingController pwdController = TextEditingController();
   
  /* -------------------------------------------------------------------------- */
  /*                                  VARIABLES                                 */
  /* -------------------------------------------------------------------------- */
  late final DataProvider _provider;
  RxBool isLoading = false.obs;
  final validator = CommonValidator();
  final formKeyRegister = GlobalKey<FormState>();
  final formKeyLogin = GlobalKey<FormState>();
  
  

  /* -------------------------------------------------------------------------- */
  /*                                 LIFECYCLES                                 */
  /* -------------------------------------------------------------------------- */

  @override
  void onReady() {
    _provider = Get.find<DataProvider>();
    super.onReady();
  }

  /* -------------------------------------------------------------------------- */
  /*                                   METHODS                                  */
  /* -------------------------------------------------------------------------- */

   Future login({required String user, required String password})async {
    //isLoading.value = true;
    if (formKeyLogin.currentState!.validate()) {
      isLoading.value =true;
    
    try {
      final login = await _provider.login(user: user, password: password);
      if(login.message == 'Success'){
        Get.offAllNamed(Routes.homePage);
      }else{
        CustomFlushbar(message: login.message!).flushbar.show(Get.context!);

      }
      
    } catch (e) {
      CustomFlushbar(message: 'Por favor intenta mas tarde').flushbar.show(Get.context!);
    }
    isLoading.value = false;
    }
  }

  Future register()async {
    if (formKeyRegister.currentState!.validate()) {
    isLoading.value = true;
    try {
      final result = await _provider.register(
        user: nameController.text, 
        lastname: lastNameController.text, 
        email: emailController.text, 
        password: pwdController.text
    );
    if(result.message == 'Success'){
      CustomFlushbar(message: 'Bienvenido, cuenta registrada con exito').flushbar.show(Get.context!);
      await 2.delay();
      nameController.clear();
      lastNameController.clear();
      emailController.clear();
      pwdController.clear();
      Get.toNamed(Routes.loginPage);
    }else{
      CustomFlushbar(message: result.message!).flushbar.show(Get.context!);

    }
  
      
    } catch (e) {
      CustomFlushbar(message: 'Por favor intenta mas tarde').flushbar.show(Get.context!);
    }
    isLoading.value = false;
    }
  }  
}
