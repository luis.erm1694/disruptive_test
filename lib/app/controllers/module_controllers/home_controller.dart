import 'package:disruptive_lerm/app/data/model/address_model.dart';
import 'package:disruptive_lerm/app/data/model/all_address_response.dart';
import 'package:disruptive_lerm/app/data/model/postal_code_model.dart';
import 'package:disruptive_lerm/app/data/provider/data_provider.dart';
import 'package:disruptive_lerm/app/data/provider/global_data.dart';
import 'package:disruptive_lerm/app/ui/widgets/custom_flushbar.dart';
import 'package:disruptive_lerm/app/utils/common_validator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddressController extends GetxController {
  /* -------------------------------------------------------------------------- */
  /*                                 CONTROLLERS                                */
  /* -------------------------------------------------------------------------- */

  TextEditingController streetController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController externalNumberController = TextEditingController();
  TextEditingController postalCodeController = TextEditingController();
  TextEditingController referencesController = TextEditingController();
  TextEditingController municipalityController = TextEditingController();
  TextEditingController colonyController = TextEditingController();
  TextEditingController stateController = TextEditingController();

  /* -------------------------------------------------------------------------- */
  /*                                  VARIABLES                                 */
  /* -------------------------------------------------------------------------- */

  late final DataProvider _provider;
  RxBool isDefault = false.obs;
  final validator = CommonValidator();
  RxBool isLoading = false.obs;
  late  Rx<PostalCodeResponseModel>? responseCp = PostalCodeResponseModel().obs;
  late  Rx<AllAddresResponse>? allAddress = AllAddresResponse().obs;
  RxInt selectedIndexDropdown = (-1).obs;
  final formKey = GlobalKey<FormState>();
  RxBool isEditing = false.obs;
  late AddressModel addressSelected;
  int indexAddres = -1;


  /* -------------------------------------------------------------------------- */
  /*                                 LIFECYCLES                                 */
  /* -------------------------------------------------------------------------- */

  @override
  void onInit() {
    _provider = Get.find<DataProvider>();
    super.onInit();
  }
  


  /* -------------------------------------------------------------------------- */
  /*                                   METHODS                                  */
  /* -------------------------------------------------------------------------- */

  String? isValidDropdown(int? value) {
    return (value != null && value != -1) ? null : 'Este campo es requerido';
  }
  void colonyChanged({required int? value}) {
    
    selectedIndexDropdown.value = value!;
    colonyController.text = responseCp!.value.sepomex![value].asentamiento ?? 'N/A';
    stateController.text = responseCp!.value.sepomex![value].estado ?? 'N/A';
    municipalityController.text = responseCp!.value.sepomex![value].municipio ?? 'N/A';
  }

  Future searchByCp({required String postalCode})async {
    isLoading.value = true;
    try {
      selectedIndexDropdown.value = -1;
      responseCp!.value = await _provider.searchByCp(postalCode: postalCode);
      if(responseCp!.value.sepomex!.isNotEmpty){
        stateController.text = responseCp!.value.sepomex![0].estado ?? 'N/A';
        municipalityController.text = responseCp!.value.sepomex![0].municipio ?? 'N/A';
      }
    } catch (e) {
      CustomFlushbar(message: 'No tenemos cobertura en esta zona\nPuedes intentar con otro código postal').flushbar.show(Get.context!);
      stateController.clear();
      colonyController.clear();
      municipalityController.clear();
    }
    isLoading.value = false;
  }

  

  void postalCodeChanged({required String postalCode}) {
    if (postalCode.length == 5) {
      searchByCp(postalCode: postalCode);
      
    }
  }

  addAddress()async{
    if (formKey.currentState!.validate() && selectedIndexDropdown.value != -1) {
      try {
      AddressModel newAddress = AddressModel(
        street: streetController.text, 
         externalNumber: externalNumberController.text, 
         postalCode: postalCodeController.text, 
         state: stateController.text, 
         municipality: municipalityController.text, 
         colony: colonyController.text, 
         references: referencesController.text, 
         isDefault:  isDefault.value? '1' : '0',
         id: '-1'
      );
      await _provider.addAddress(address: newAddress, idUser: int.parse(GlobalMemory.user!.idUser!));
      //myAddress.add(newAddress);
      isDefault.value = false;
      CustomFlushbar(message: 'Se ha agregado ${nameController.text} correctamente').flushbar.show(Get.context!);
      clearFields();
      } catch (e) {
        CustomFlushbar(message: 'Ocurrio un error intenta nuevamente').flushbar.show(Get.context!);
        isEditing.value = false;
        
        
      }
      isLoading.value =false;
    }
  }

  clearFields(){
    responseCp?.value = PostalCodeResponseModel(sepomex: null);
    selectedIndexDropdown.value = -1;
    nameController.clear();
    referencesController.clear();
    streetController.clear();
    externalNumberController.clear();
    postalCodeController.clear();
    municipalityController.clear();
    colonyController.clear();
    stateController.clear();
    isDefault.value = false;
  }

  Future deleteAddress({required int index})async{

    isLoading.value = true;
    try {
      await _provider.deleteAddress(addressId: addressSelected.id);
      clearFields();
      allAddress?.value =  await _provider.getAddress(userId: addressSelected.idUSer!);
      
    } catch (e) {
      CustomFlushbar(message: e.toString()).flushbar.show(Get.context!);
    }
    isLoading.value = false;
  
    update();
  }

  setAddressFields(){
    referencesController.text = addressSelected.references;
    streetController.text = addressSelected.street;
    externalNumberController.text = addressSelected.externalNumber.toString();
    postalCodeController.text = addressSelected.postalCode;
    municipalityController.text = addressSelected.municipality;
    colonyController.text = addressSelected.colony;
    stateController.text = addressSelected.state;
    addressSelected.isDefault == '1'? isDefault.value = true : false;
    Get.back();

    update();

  }

  editAddress()async{
    if (formKey.currentState!.validate()) {
      try {
      isLoading.value = true;
      final updateAddress = AddressModel(
        id: addressSelected.id,
        street: streetController.text, 
         externalNumber: externalNumberController.text, 
         postalCode: postalCodeController.text, 
         state: stateController.text, 
         municipality: municipalityController.text, 
         colony: colonyController.text, 
         references: referencesController.text, 
         isDefault:  isDefault.value? '1' : '0',
      );
      final response  = await _provider.updateAddress(address: updateAddress);
      if(response.message == 'Success'){
        CustomFlushbar(message: 'Se editó ${nameController.text} correctamente').flushbar.show(Get.context!);
        isEditing.value = false;
        clearFields();
       
      }else{
        CustomFlushbar(message: response.message!).flushbar.show(Get.context!);
      }
      
    }catch (e) {
      CustomFlushbar(message:'Algo salió mal, por favor intente nuevamente').flushbar.show(Get.context!);
      }
    isLoading.value = false;
  }
}

}
