import 'package:disruptive_lerm/app/controllers/module_controllers/home_controller.dart';
import 'package:disruptive_lerm/app/data/provider/data_provider.dart';
import 'package:disruptive_lerm/app/data/provider/global_data.dart';
import 'package:get/get.dart';

class AddressStateMixin extends GetxController with StateMixin {
  final AddressController _addressController = Get.find<AddressController>();

  @override
  void onInit() {
    change(null, status: RxStatus.empty());
    runAll();
    super.onInit();
  }

  void runAll() async {
    change(null, status: RxStatus.loading());
    try {
      final DataProvider provider = Get.put(DataProvider());
      final  data = await provider.getAddress(userId: GlobalMemory.user!.idUser!);
      _addressController.allAddress?.value = data;
      change(null, status: RxStatus.success());
    } catch (error) {
      change(null, status: RxStatus.error(error.toString()));
    }
      
  }
   
}



