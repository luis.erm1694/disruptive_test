
import 'package:get/get_utils/get_utils.dart';

class CommonValidator {
  /// Validations use in forms
  ///

  

  String? isRequired(String? value) {
    if (value != null) {
      if (value.isEmpty) {
        return 'Este campo es requerido';
      }
    }
    return null;
  }

  String? isValidDropdown(int? value) {
    return (value != null && value != -1) ? null : 'Este campo es requerido';
  }

  String? isValidEmail(String? value) {
    if(value != null) {
      if(!value.isEmail) {
        return 'Coloca un email válido';
      }
    }
    return null;
  }

  
}
