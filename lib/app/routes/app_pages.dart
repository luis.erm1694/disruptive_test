
import 'package:disruptive_lerm/app/bindings/account_binding.dart';
import 'package:disruptive_lerm/app/bindings/home_binding.dart';
import 'package:disruptive_lerm/app/ui/pages/home/address/all_address_page.dart';
import 'package:disruptive_lerm/app/ui/pages/home/home_page.dart';
import 'package:disruptive_lerm/app/ui/pages/login/login_page.dart';
import 'package:disruptive_lerm/app/ui/pages/register.dart/register.page.dart';
import 'package:disruptive_lerm/app/ui/pages/splash_page/splash_page.dart';
import 'package:get/get.dart';
part 'app_routes.dart';

class AppPages {
  static final List<GetPage> routes = <GetPage>[
    GetPage(name: Routes.splashPage, page: () => const SplashPage()),
    GetPage(name: Routes.homePage, page: () => const HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.addressPage, page: () =>  AllAddressPage(), binding: HomeBinding()),
    GetPage(name: Routes.registerPage, page: () => const RegisterPage(), binding: AccountBinding()),
    GetPage(name: Routes.loginPage, page: () => const LoginPage(), binding: AccountBinding()),
  ];
}
