import 'package:disruptive_lerm/app/controllers/module_controllers/home_controller.dart';
import 'package:disruptive_lerm/app/controllers/state_mixin_controllers/address_state_mixin.dart';
import 'package:get/get.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<AddressController>(AddressController());
    Get.lazyPut<AddressStateMixin>(() => AddressStateMixin());
  }
}
