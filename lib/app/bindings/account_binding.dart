import 'package:disruptive_lerm/app/controllers/module_controllers/account_controller.dart';
import 'package:get/get.dart';

class AccountBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<AccountController>(AccountController());
  }
}
