import 'package:disruptive_lerm/app/controllers/module_controllers/account_controller.dart';
import 'package:disruptive_lerm/app/ui/widgets/custom_textfield.dart';
import 'package:disruptive_lerm/app/utils/assets_path.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<AccountController> {
  const LoginPage({Key? key}) : super(key: key);

@override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
           decoration: const BoxDecoration(
             image: DecorationImage(
               image: AssetImage(AssetsPath.poster),
               fit: BoxFit.cover,
             ),
            ),
          ),
          Container(
            
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
            height: Get.height,
            child: SingleChildScrollView(
              child: Obx(() => Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      const  SizedBox(height: 40,),
                    const Text('Login',
                       style:  TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
                     ),
                      Form(
                        key: controller.formKeyLogin,
                        child: Column(
                          children: [
                            CustomTextField(
                              controller: controller.emailController,
                              validator: controller.validator.isValidEmail,
                              title: 'email',
                               keyboardType: TextInputType.emailAddress,

                            ),
                            CustomTextField(
                              controller: controller.pwdController,
                              validator: controller.validator.isRequired,
                              title: 'Contraseña',
                            ),
                            const SizedBox(height: 20,),
                            MaterialButton(
                              child: Container(
                                width: Get.width*.45,
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white, width: 2),
                                  borderRadius: BorderRadius.circular(8),
                                  color: Colors.transparent
                                ),
                                child: const Center(
                                  child: Text(
                                    'Ingresar',
                                    style:  TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
                                  ),
                                )
                              ),
                              onPressed: (){
                                controller.login(user: controller.emailController.text, password: controller.pwdController.text);
                              }
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: controller.isLoading.value,
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                ],
              )
              ),
            ),
          ),
        ],
      ),
    );
  }
}