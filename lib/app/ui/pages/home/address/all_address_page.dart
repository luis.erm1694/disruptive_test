import 'dart:io';

import 'package:disruptive_lerm/app/controllers/module_controllers/home_controller.dart';
import 'package:disruptive_lerm/app/controllers/state_mixin_controllers/address_state_mixin.dart';
import 'package:disruptive_lerm/app/ui/pages/home/widget/list_address.dart';
import 'package:disruptive_lerm/app/utils/assets_path.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AllAddressPage extends GetView<AddressController> {
  AllAddressPage({super.key});

  final infoAddress = Get.find<AddressStateMixin>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Get.width,
        decoration: const BoxDecoration(
         image: DecorationImage(
            image: AssetImage(AssetsPath.poster),
            fit: BoxFit.cover,
          ),
        ),
        padding: const EdgeInsets.all(20),
        height: Get.height,
        child: infoAddress.obx((info)=> Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 40,),
            IconButton(
              onPressed: () {
                controller.isEditing.value = false;
                Get.back();


              },
              icon: Icon(
                Platform.isAndroid?
                Icons.arrow_back
                : Icons.chevron_left,
                color: Colors.white,
                size: 40,
              )
            ),
            const Center(child: ListAddressWidget()),
          ],
        ),
        onLoading: const Center(child: CircularProgressIndicator(),),
        onError: (error) =>  Text(error.toString())
        )
      ),
    );
  }
}

