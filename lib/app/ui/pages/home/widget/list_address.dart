

import 'package:disruptive_lerm/app/controllers/module_controllers/home_controller.dart';
import 'package:disruptive_lerm/app/ui/pages/home/widget/addres_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListAddressWidget extends GetView<AddressController> {
  //final CashbackModel infoLevel;
  const ListAddressWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.allAddress != null && controller.allAddress!.value.addressList!.isEmpty
      ? Column(
        children: [
          SizedBox(height:  Get.height*.1,),
          const Center(
            child: Text('Aqui podrás ver las direcciones que has agregado',
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
              textAlign: TextAlign.center,
            
            )
          ),
        ],
      )
      : Obx(() => Expanded(
        child: ListView.builder(
          padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemCount: controller.allAddress!.value.addressList!.length,
            itemBuilder: (BuildContext context, int index) {
              return AddressWidget(
                address: controller.allAddress!.value.addressList![index],
                index: index,
              );
            },
          ),
      )),
    );
  }
}