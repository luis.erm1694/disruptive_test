import 'package:disruptive_lerm/app/controllers/module_controllers/home_controller.dart';
import 'package:disruptive_lerm/app/data/provider/global_data.dart';
import 'package:disruptive_lerm/app/routes/app_pages.dart';
import 'package:disruptive_lerm/app/ui/theme/colors.dart';
import 'package:disruptive_lerm/app/ui/widgets/custom_textfield.dart';
import 'package:disruptive_lerm/app/utils/assets_path.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class HomePage extends GetView<AddressController> {
  const HomePage({super.key});
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          RotatedBox(
            quarterTurns: 1,
             child:  Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AssetsPath.poster),
                  fit: BoxFit.cover,
                ),
               ),
             ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
            height: Get.height,
            child: SingleChildScrollView(
              child: Obx(() => Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                    const  SizedBox(height: 40,),
                    const Text('Dirección',
                       style:  TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
                     ),
                      Form(
                        key: controller.formKey,
                        child: Column(
                          children: [
                             CustomTextField(
                              validator: controller.validator.isRequired,
                              controller: controller.streetController,
                              title: 'Calle',
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(128),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children:[
                                CustomTextField(
                                  validator: controller.validator.isRequired,
                                  controller: controller.postalCodeController,
                                  width: Get.width*.42,
                                  title: 'C.P',
                                  keyboardType: TextInputType.phone,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(5),
                                  ],
                                  onChanged: (value) => controller.postalCodeChanged(postalCode: value),
                                ),
                                CustomTextField(
                                  controller: controller.externalNumberController,
                                  width: Get.width*.42,
                                  keyboardType: TextInputType.phone,
                                  validator: controller.validator.isRequired,
                                  title: 'Número exterior',
                                )
                              ],
                            ),
                            CustomTextField(
                              readOnly: true,
                              controller: controller.municipalityController,
                              validator: controller.validator.isRequired,
                              title: 'Municipio',
                            ),
                            CustomTextField(
                              readOnly: true,
                              controller: controller.stateController,
                              validator: controller.validator.isRequired,
                              title: 'Estado',
                            ),
                            controller.responseCp?.value.sepomex == null?
                            CustomTextField(
                              readOnly: true,
                              controller: controller.colonyController,
                              validator: controller.validator.isRequired,
                              title: 'Asentamiento',
                            )
                            : Container(
                              margin: const EdgeInsets.only(top:30),
                              width: Get.width,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5)
                              ),
                              child: Center(
                                child: DropdownButtonFormField<int>(
                                  validator: controller.validator.isValidDropdown,
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  isExpanded: true,
                                  focusColor: Colors.white,
                                  value: controller.selectedIndexDropdown.value == -1 ? null : controller.selectedIndexDropdown.value,
                                  iconEnabledColor: Colors.black,
                                  hint: const Padding(
                                    padding:  EdgeInsets.only(left:5.0),
                                    child:  Text('Asentamiento'),
                                  ),
                                  items:  [
                                    for(int i=0; i<controller.responseCp!.value.sepomex!.length; i++) ...[
                                      DropdownMenuItem<int>(
                                        value: i,
                                        child: Padding(
                                          padding: const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            controller.responseCp?.value.sepomex![i].asentamiento ?? 'N/A',
                                            style: const TextStyle(color: CustomColors.btnPrimary, fontWeight: FontWeight.w900, fontSize: 16),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ],
                                   onChanged: (value) => controller.colonyChanged(value: value),
                                ),
                              ),
                            ),
                            CustomTextField(
                              controller: controller.referencesController,
                              validator: controller.validator.isRequired,
                              title: 'Referencias',
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(256),
                              ],
                            ),
                            const SizedBox(height: 20,),
                            Row(
                              children: [
                                CupertinoSwitch(
                                  value: controller.isDefault.value,
                                  onChanged: (bool newValue) {
                                    controller.isDefault.value = newValue;
                                  },
                                  activeColor: const Color.fromARGB(255, 111, 212, 232),
                                  trackColor: CupertinoColors.systemGrey5,
                                ),
                                const SizedBox(width: 15,),
                                const Expanded(
                                  child: Text(
                                    'Seleccionar esta dirección como predeterminada', 
                                    softWrap: true,
                                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 20,),
                            MaterialButton(
                              child: Container(
                                width: Get.width*.6,
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white, width: 2),
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.transparent
                                ),
                                child: Center(
                                  child: Text(
                                    controller.isEditing.value? 'Editar' :'Agregar',
                                    style: const  TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
                                  ),
                                )
                              ),
                              onPressed: (){
                                controller.isEditing.value? controller.editAddress() : controller.addAddress();
                              }
                            ),
                            const SizedBox(height: 20,),
                            MaterialButton(
                              child: Container(
                                width: Get.width*.6,
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white, width: 2),
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.transparent
                                ),
                                child: const Center(
                                  child: Text(
                                    'Ver mis direcciones',
                                    style:  TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
                                  ),
                                )
                              ),
                              onPressed: (){
                                Get.toNamed(Routes.addressPage);
                              }
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: controller.isLoading.value,
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                ],
              )
              ),
            ),
          ),
          Positioned(
            top: 40,
            right: 20,
            child: IconButton(
              tooltip: 'Cerrar sesión',
              icon: const Icon(
                Icons.exit_to_app,
                color: Colors.white,
                size: 25,
              ),
              onPressed: () {
                GlobalMemory.box!.erase();
                GlobalMemory.user = null;
                Get.offAllNamed(Routes.registerPage);
                
              },
            )
          ),
        ],
      ),
    );
  }
}
