import 'package:disruptive_lerm/app/controllers/module_controllers/splash_controller.dart';
import 'package:disruptive_lerm/app/ui/theme/colors.dart';
import 'package:disruptive_lerm/app/utils/assets_path.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashPage extends GetView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.dark,
      body: Container(
        height: Get.height,
        width: Get.width,
        decoration: const BoxDecoration(
         image: DecorationImage(
            image: AssetImage(AssetsPath.poster),
            fit: BoxFit.cover,
          ),
        ),
        
      ),
    );
  }
}
