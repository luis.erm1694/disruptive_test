import 'package:disruptive_lerm/app/ui/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'colors.dart';

/// Build a [CustomTheme] for [ThemeData] that's used to configure a [Theme].
class CustomTheme {
  static final ThemeData defaultTheme = ThemeData(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    scaffoldBackgroundColor: CustomColors.light,
    appBarTheme: const AppBarTheme(
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
    ).copyWith(
      elevation: 0.0,
      color: CustomColors.light,
      titleTextStyle: const TextStyle(
        color: CustomColors.dark,
        fontWeight: FontWeight.bold,
        fontSize: CustomStyles.xLargeFontSize,
      ),
      iconTheme: const IconThemeData().copyWith(
        color: CustomColors.dark,
      ),
    ),
    inputDecorationTheme: const InputDecorationTheme(
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: CustomColors.dark,
        ),
      ),
      labelStyle: TextStyle(color: CustomColors.dark),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: CustomColors.dark),
      ),
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: CustomColors.dark,
        ),
      ),
    ),
    primarySwatch: CustomColors.darkMaterialColor,
  );
}
