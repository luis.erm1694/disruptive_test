import 'package:disruptive_lerm/app/ui/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({super.key, this.controller, this.onChanged, this.width, this.title, this.readOnly, this.keyboardType, this.inputFormatters, this.validator, this.initialValue});
  final TextEditingController? controller;
  final ValueChanged<String>? onChanged;
  final double? width;
  final String? title;
  final bool? readOnly;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final String? Function(String?)? validator;
  final String? initialValue;



  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top:30),
      width:width ?? Get.width,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: const Color.fromARGB(255, 2, 45, 81), width: 2),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Center(
        child: TextFormField(
          initialValue: initialValue,
          readOnly: readOnly ?? false,
          controller: controller,
          onChanged: onChanged,
          validator: validator,
          inputFormatters: inputFormatters,
          keyboardType: keyboardType,
          style: const TextStyle(color: CustomColors.btnPrimary, fontWeight: FontWeight.w900, fontSize: 16),
          scrollPadding: EdgeInsets.zero,
          decoration: InputDecoration(
            //errorText: snapshot.error,
            contentPadding: const EdgeInsets.all(8),
            border: InputBorder.none,
            hintText: title??'',
            hintStyle:  TextStyle(
              color: CustomColors.btnPrimary.withOpacity(.6), 
              fontWeight: FontWeight.w900, 
              fontSize: 14
            ),
            
          ),
          cursorColor: CustomColors.btnPrimary
        ),
      ),
    );
  }
}
